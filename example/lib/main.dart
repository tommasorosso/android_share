import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart';
import 'package:android_share/android_share.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {

      final path = (await getTemporaryDirectory()).path + '/dump.dat' ;
      await writeFile(path, "diao");
      platformVersion = await AndroidShare.platformVersion(path);
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('Plugin example app'),
        ),
        body: new Center(
          child: new Text('Running on: $_platformVersion\n'),
        ),
      ),
    );
  }
}


Future<Result<Nothing>> writeFile(String name, String contents) async {
  var file = File(name);
  file.createSync(recursive: true);
  file.writeAsStringSync(contents);
  return file.readAsStringSync() == contents ?  Result.Ok(Nothing()) : Result.Err('Error writing file');
}

class Result<T> {
  final bool ok;
  final T item;
  final String err;

  Result.Ok(T item) : ok = true, item = item, err = null;
  Result.Err(String error) : ok = false, item = null, err = error;

  static Result<U> from_unsafe_fn<U>(U Function() f, { String errorMessage }) {
    try { return Result.Ok(f()); }
    catch(e) { return Result.Err(errorMessage ?? e.toString()); }
  }

  T unwrap_or_default(T value) => ok ? item : value;
  T unwrap() => ok ? item : throw 'Panic while unwrapping value err:$err';
  T unwrap_or_else(T Function(String) f) => ok ? this.item : f(err);
  T expect(String message) => ok ? item : throw message;

  String unwrap_err() => ok ? throw 'Panic, found ok expected err' : err;
  String expect_err(String message) => ok ? throw message : err;

  //  Monadic and functor behaviour
  Result<V> map<V>(V Function(T) f) => ok ? Result.Ok(f(item)) : Result.Err(err);
  Result<T> map_err(String Function(String) f) => ok ? this : Result.Err(f(err));
  Result<U> and_then<U>(Result<U> Function(T) f) => ok ? f(item) : this;

  Result<T> or(Result<T> r) => ok ? this : r;
  Result<T> or_else(Result<T> Function() f) => ok ? this : f();

  bool is_ok() => ok;
  bool is_err() => !is_ok();

  Result<T> operator |(T Function(T) f) => this.map(f);
}

class Nothing {}
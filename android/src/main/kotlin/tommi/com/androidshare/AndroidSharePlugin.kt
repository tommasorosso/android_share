package tommi.com.androidshare

import android.content.Intent
import android.net.Uri
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.PluginRegistry.Registrar
import android.app.Activity
import android.content.Context
import java.io.BufferedReader
import java.io.File
import android.os.StrictMode
import android.os.Build
import android.support.v4.content.FileProvider

class AndroidSharePlugin(val activity: Activity) : MethodCallHandler {

    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar): Unit {

//            StrictMode.VmPolicy.Builder().apply { StrictMode.setVmPolicy(this.build()) }

            val channel = MethodChannel(registrar.messenger(), "android_share")
            channel.setMethodCallHandler(AndroidSharePlugin(registrar.activity()))
        }
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        if (call.method.equals("getPlatformVersion")) {
            val path = call.argument<String>("path")
            val uri = Uri.decode(path)
            val file = File(uri)
            val usableUri = FileProvider.getUriForFile(activity, "provider.android", file)

            if (!file.exists()) {
                result.success("Terrible failure")
                return
            }
            try {
                val intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    putExtra(Intent.EXTRA_STREAM, usableUri)
                    type = "*/*"
                }

                activity.startActivityForResult(intent, 99)
            } catch (e:Exception) {
                result.success(e.toString())
                return
            }


            result.success("()")
        } else {
            result.notImplemented()
        }
    }
}

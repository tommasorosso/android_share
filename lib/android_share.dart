import 'dart:async';

import 'package:flutter/services.dart';

class AndroidShare {
  static const MethodChannel _channel =
      const MethodChannel('android_share');

  static Future<String> platformVersion(String path) async {
    final String version = await _channel.invokeMethod('getPlatformVersion', { 'path': path });
    return version;
  }
}

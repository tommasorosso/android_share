Installation
____________

- 1 Open android folder of your project.
- 2 Paste this
```
<provider
    android:name="android.support.v4.content.FileProvider"
    android:authorities="provider.android"
    android:exported="false"
    android:grantUriPermissions="true">
    <meta-data
        android:name="android.support.FILE_PROVIDER_PATHS"
        android:resource="@xml/provider_paths"/>
</provider>
```
inside your `<application>` tag
- 3 create a folder on `app/res` called `xml` -> `app/res/xml`
- 4 copy this inside `provider_paths.xml`
```
<?xml version="1.0" encoding="utf-8"?>
<paths xmlns:android="http://schemas.android.com/apk/res/android">
    <root-path name="root" path="." />
    <external-path name="external_files" path="."/>
    <files-path
        name="aaaaaaaa"
        path="."/>
</paths>
```
- 5 inside `build.gradle` (app) add as dependency: `implementation 'com.android.support:support-v4:27.1.1'`

#import "AndroidSharePlugin.h"
#import <android_share/android_share-Swift.h>

@implementation AndroidSharePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAndroidSharePlugin registerWithRegistrar:registrar];
}
@end
